class nli_iserver::params {
  $jp2_cache_dir                                     = "/jpeg2k_cache"
  $jp2_cache_share_name                       = "/jpeg2k_cache"
  $kakadu_install_files_mount_point         = "/mnt/install_files"
  $kakadu_install_files_source_share        = "localhost:/install_files"
}
