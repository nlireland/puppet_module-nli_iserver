class nli_iserver (
  $jp2_cache_dir                                     = $nli_iserver::params::jp2_cache_dir,
  $jp2_cache_share_name                       = $nli_iserver::params::jp2_cache_share_name,
  $kakadu_install_files_mount_point         = $nli_iserver::params::kakadu_install_files_mount_point,
  $kakadu_install_files_source_share        = $nli_iserver::params::kakadu_install_files_source_share
  ) inherits nli_iserver::params {

  include 'epel'
  include 'java'

  file {"jp2_cache_dir":
    path => "$jp2_cache_dir",
    ensure => "directory",
  }

  include nfs::client
  class { 'nfs::server':
    package => latest,
    service => running,
    enable  => true,
  }
  nfs::export { "$jp2_cache_share_name":
      options => [ 'rw', 'async', 'no_root_squash' ],
      clients => [ "172.16.1.1/24" ],
      require => File["jp2_cache_dir"],
  }

  #nfs packages needed for mount
  package {'libnfsidmap':
    ensure => 'installed',
  }

  file  {"kakadu_install_files_mount_point":
    path => "$kakadu_install_files_mount_point",
    ensure => "directory",
  }
  mount { "kakadu_source_mount":
    name => "$kakadu_install_files_mount_point",
    device  => "$kakadu_install_files_source_share",
    fstype  => "nfs",
    ensure  => "mounted",
    options => "defaults",
    atboot  => true,
    require => [File["kakadu_install_files_mount_point"], Package["nfs-utils", "libnfsidmap"]],
  }

#  file { "/etc/profile.d/java_home.sh"
#    content => 'export JAVA_HOME="/usr/lib/jvm/java-openjdk"',
#  }

#  package { 'java-1.6.0-openjdk-devel':
#      ensure      => installed,
#      provider    => apt,
#      require     => Package['apt'];
#  }

  package { 'gcc':
      ensure      => installed,
  }

  package { 'gcc-c++':
      ensure      => installed,
  }

  file {'kakadu_sdk':
    ensure          => 'file',
    path             => "/usr/local/kakadu_sdk.zip",
    source          => "$kakadu_install_files_mount_point/v6_4_1-01222N.zip",
    require         => Mount["kakadu_source_mount"],
    before          => Exec["unzip_kakadu_sdk"];
  }

  exec {'unzip_kakadu_sdk':
    command    =>"unzip /usr/local/kakadu_sdk.zip && mv /usr/local/v6_4_1-01222N/ /usr/local/kakadu_sdk/",
    cwd             =>"/usr/local/",
    path            =>"/usr/bin",
    creates        => "/usr/local/kakadu_sdk/",
    require        => [File["kakadu_sdk"],Package["unzip"]],
    before         => Exec["compile_kakadu"];
  }

  exec { 'compile_kakadu':
      command      => "make -f Makefile-Linux-x86-64-gcc",
      environment  => "JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk",
      #creates          => "?",
      path              => "/usr/bin",
      cwd               => "/usr/local/kakadu_sdk/make",
      require          => [Class['java'],Package['gcc', 'gcc-c++'], Exec["unzip_kakadu_sdk"]];
  }

  #memcached
  package { 'memcached':
    ensure      => installed,
  }

  package { 'libmemcached':
    ensure      => installed,
  }

  package { 'libmemcached-devel':
    ensure      => installed,
  }

  #iipImage Server
  package{'wget':
    ensure      => installed,
  }
  package{'libtool':
    ensure      => installed,
  }
  package{'autoconf':
    ensure      => installed,
  }
  package{'automake':
    ensure      => installed,
  }
  package{'zlib-devel':
    ensure      => installed,
  }
  package{'libtiff-devel':
    ensure      => installed,
  }
  package{'libjpeg-turbo-devel':
    ensure      => installed,
    require      => Class["epel"],
  }
  package{'zip':
    ensure      => installed,
  }
  package{'unzip':
    ensure      => installed,
  }


  exec {'unpack_iipimage_server':
    command => "wget https://github.com/ruven/iipsrv/archive/master.zip && unzip master.zip && rm master.zip;",
    cwd          => '/tmp',
    path         => '/usr/bin',
    creates     => '/tmp/iipsrv-master',
    require     => Package['wget', 'unzip'],
    onlyif        => ["test ! -f /var/www/fcgi-bin/iipsrv.fcgi"],
    before      => Exec['install_iipimage_server'],
  }

  file {'/var/www':
    ensure => "directory"
  }
  file {"/var/www/fcgi-bin":
    ensure => "directory",
    require => File["/var/www"];
  }

  exec {'install_iipimage_server':
    command => "/tmp/iipsrv-master/autogen.sh && /tmp/iipsrv-master/configure --with-kakadu=/usr/local/kakdu_sdk && make && make install && mv /tmp/iipsrv-master/src/iipsrv.fcgi /var/www/fcgi-bin/ && rm -rf /tmp/iipsrv-master;",
    cwd          => '/tmp/iipsrv-master',
    path        => '/usr/bin',
    creates     => '/var/www/fcgi-bin/iipsrv.fcgi',
    require     => [File["/var/www/fcgi-bin"],Package['libtool', 'autoconf', 'automake', 'zlib-devel', 'libtiff-devel', 'libjpeg-turbo-devel']],
  }

  #lighttpd
  package{ 'lighttpd':
    ensure      => installed,
    require      => Class["epel"],
  }
  package{'lighttpd-fastcgi':
    ensure      => installed,
    require      => Class["epel"],
  }
  package{'spawn-fcgi':
    ensure      => installed,
    require      => Class["epel"],
  }
  
  # ensure that the jpeg2k_cach directory exists and apply the correct
  # selinux file_context permission that will allow the lighttpd process 
  # to access the jp2 and iiif manifest files while the server is using
  # 'enforcing' mode
  file {'security_permissions_lighttpd':
    path     => "$jp2_cache_dir",
    ensure   => "directory",
    recurse  => true,
    selrange => "s0",
    selrole  => "object_r",
    seluser  => "unconfined_u",
    seltype  => "httpd_sys_content_t",
  }
  #configeration
  #file {'/etc/lighttpd/lighttpd.conf':
  #  ensure => 'file',
  #  source => 'puppet:///modules/nli_iserver/lighttpd.conf',
  #}

#  exec { 'start lighttpd':
 #   command => "lighttpd -D -f /etc/lighttpd/lighttpd.conf",
 #   cwd          => "/usr/local/bin"
 # }

 #php

}
